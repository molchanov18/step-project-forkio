# Step-Project-Forkio

Студент № 1 - Дюг Роман, 
Студент № 2 - Молчанов Даниил

Список используемых техноголий: 
    browser-sync,
    del,
    gulp,
    gulp-autoprefixer,
    gulp-clean-css,
    gulp-file-include,
    gulp-group-css-media-queries,
    gulp-imagemin,
    gulp-js-minify,
    gulp-rename,
    gulp-sass,
    gulp-uglify-es,

Задание №1 выполнял Роман Дюг,
Задание №2 выполнял Даниил Молчанов. 
Сборкой gulp занимались вместе